resource "aws_instance" "name" {
 ami = 
 terraform {
   required_providers {
     sematext = {
       source = "sematext/sematext"
       version = ">=0.1.10"
     }
   }
 }
 
 provider "sematext" {
   sematext_region = "US"
 }
 
 resource "sematext_monitor_akka" "mymonitor" {
   name = "my monitor name"
   billing_plan_id = <[plan id](../guides/plans.md)>
   apptoken {
     name "my apptoken name"
     create_missing true
   }
 }

}